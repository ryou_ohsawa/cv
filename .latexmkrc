#!/usr/bin/env perl
$latex         = 'uplatex %O %S -halt-on-error';
$latex_silent  = 'uplatex %O %S -halt-on-error -interaction="nonstopmode"';
$dvipdf        = 'dvipdfmx %O-o %D %S';
$max_repeat    = 5;
$pdf_mode      = 3;
$pdf_previewer = "evince";
$pvc_view_file_via_temporary = 0;
# $success_cmd   = 'pdftk cv_head.pdf body.pdf cat output ../application.pdf';
