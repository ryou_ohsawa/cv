BIBS    := $(wildcard bibliography_*.bib)
RECORDS := $(subst bib,tex,$(subst bibliography,records,$(BIBS)))
TARGETS := cv_ja.pdf
all: $(TARGETS)

.PHONY: all bibsync clean


$(RECORDS): records_%.tex: bibliography_%.bib bibconvert.py
	./bibconvert.py $< $@

$(TARGETS): %.pdf: %.tex cv_style.sty $(RECORDS)
	latexmk $<


BIBTGT := /home/rohsawa/Documents/info/web/data
bibsync:
	rsync -avvu $(BIBTGT)/*.bib .


clean:
	rm -f *.dvi *.aux *.log *.out *.fls *.fdb_latexmk
	rm -f $(RECORDS)
	rm -f $(TARGETS)
