#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
from bibtexparser.bparser import BibTexParser
import bibtexparser as bp
import re


reg_fullname = re.compile(r'^{.*}$')
reg_mathmode = re.compile(r'\$([^$]+)\$')


def make_boldmath(string):
  return reg_mathmode.sub(r'$\\bm{\1}$', string)


def get_initial(given):
  res = re.match(r'^\\[A-Z].*$', given)
  if res is not None:
    return given[0:2]
  res = re.match(r'^{\\[A-Z]}.*$', given)
  if res is not None:
    return given[0:4]
  res = re.match(r'^\\.{[A-Z]}.*$', given)
  if res is not None:
    return given[0:6]
  res = re.match(r'^\\.{[A-Z]}.*$', given)
  if res is not None:
    return given[0:6]
  res = re.match(r'^{\\.[A-Z]}.*$', given)
  if res is not None:
    return given[0:6]
  res = re.match(r'^{\\.{[A-Z]}}.*$', given)
  if res is not None:
    return given[0:8]
  return given[0]


def initialize_handler(name, fmt='{0:}, {1:}'):
  if isinstance(name, list) or isinstance(name, tuple):
    if len(name)==1: return str(name[0])
    if len(name)!=2:
      raise ValueError('name {} is not properly parsed'.format(name))
    family, given = name
    given = ' '.join(['{}.'.format(get_initial(n.strip())) for n in given.split()])
    return fmt.format(family, given)
  else:
    return str(name)


def parse_author(
    author_text, handler=initialize_handler,
    max_authors=None, highlight=None):
  author_list = list()
  for author in author_text.split(' and '):
    if reg_fullname.match(author):
      author_list.append(author.strip())
    else:
      author_list.append([p.strip() for p in author.split(',')])
  author_list = [handler(a) for a in author_list]

  if max_authors and len(author_list)>max_authors:
    author_list = author_list[0:max_authors]
    author_text = ', '.join(author_list)
    return '{}, {\\textit{et al.}}'.format(author_text)
  else:
    return ', '.join(author_list)


def process_article(entry, max_authors=None):
  title = make_boldmath(entry['title'])
  journal = entry['journal']
  year = entry['year']
  volume = entry['volume']
  pages = entry['pages']
  authors = parse_author(entry['author'], max_authors=max_authors)
  bib = '\\textbf{{{}}},\\\\[5pt plus 3pt minus 3pt]'.format(title)
  bib += '\\noindent\\parbox{\\linewidth}{\\small '
  bib += '{}, {} ({})'.format(authors, journal, year)
  if volume: bib += ', {}'.format(volume)
  if pages: bib += ', {}'.format(pages)
  return bib + '}'


def process_proceedings(entry, max_authors=None):
  title = make_boldmath(entry['title'])
  booktitle = entry['booktitle']
  year = entry['year']
  volume = entry.get('volume')
  pages = entry.get('pages')
  authors = parse_author(entry['author'], max_authors=max_authors)
  bib = '\\textbf{{{}}},\\\\[5pt plus 3pt minus 3pt]'.format(title)
  bib += '\\noindent\\parbox{\\linewidth}{\\small '
  bib += '{}, {} ({})'.format(authors, booktitle, year)
  if volume: bib += ', {}'.format(volume)
  if pages: bib += ', {}'.format(pages)
  return bib + '}'


def process_conference(entry, max_authors=None):
  title = make_boldmath(entry['title'])
  presentation = entry['type']
  date = entry['date']
  venue = entry['venue']
  conference = entry['conference']
  authors = parse_author(entry['author'], max_authors=max_authors)
  bib = '\\textbf{{{}}},\\\\[5pt plus 3pt minus 3pt]'.format(title)
  bib += '\\noindent\\parbox{\\linewidth}{\\small '
  bib += '{}, {} '.format(authors, conference)
  bib += '({}; {}, {})'.format(presentation, venue, date)
  return bib + '}'


def process_grants(entry, max_authors=None):
  title = make_boldmath(entry['title'])
  source = entry['type']
  date = entry['date']
  authors = parse_author(entry['author'], max_authors=max_authors)
  bib = '\\textbf{{{}}},\\\\[5pt plus 3pt minus 3pt]'.format(title)
  bib += '\\noindent\\parbox{\\linewidth}{\\small '
  bib += '{}, {} ({})'.format(authors, source, date)
  return bib + '}'


def process_misc(entry, max_authors=None):
  title = make_boldmath(entry['title'])
  event = entry['type']
  organization = entry.get('organization')
  date = entry['date']
  url  = entry.get('url')
  authors = parse_author(entry['author'], max_authors=max_authors)
  bib = '\\textbf{{{}}},\\\\[5pt plus 3pt minus 3pt]'.format(title)
  bib += '\\noindent\\parbox{\\linewidth}{\\small '
  bib += '{} '.format(authors)
  if organization is not None:
    bib += '({}; {}, {})'.format(event, organization, date)
  else:
    bib += '({}; {})'.format(event, date)
  if url is not None:
    bib += '\\\\\n\\url{{{}}}'.format(url)
  return bib + '}'




if __name__ == '__main__':
  parser = ap(description='convert .bib into .tex file')
  parser.add_argument(
    'bib', type=str, help='input .bib file')
  parser.add_argument(
    'tex', type=str, help='output .tex file')

  args = parser.parse_args()
  btp = BibTexParser()
  btp.ignore_nonstandard_types = False

  with open(args.bib, 'r') as bib:
    db = bp.load(bib, btp)


  with open(args.tex, 'w') as tex:
    tex.write('\\begin{thebibliography}{99}\n\n')

    for n,entry in enumerate(db.entries):
      tex.write('\\bibitem[\\Alph{{subsection}}-{}]'.format(n+1))
      tex.write('{{{}}} '.format(entry['ID']))
      if entry['ENTRYTYPE'].lower()=='article':
        formatted = process_article(entry)
      elif entry['ENTRYTYPE'].lower()=='inproceedings':
        formatted = process_proceedings(entry)
      elif entry['ENTRYTYPE'].lower()=='conference':
        formatted = process_conference(entry)
      elif entry['ENTRYTYPE'].lower()=='grants':
        formatted = process_grants(entry)
      elif entry['ENTRYTYPE'].lower()=='misc':
        formatted = process_misc(entry)
      tex.write('{}\n'.format(formatted))

    tex.write('\\end{thebibliography}\n')
